#! /bin/sh

HOME_INTERFACE=${__HOME_INTERFACE}
INTERNAL_INTERFACE=${__PRIVATE_INTERFACE}
EXTERNAL_INTERFACE=tun0
PROTO=udp
DDNS_DOMAIN=minkebox.net
PRIVATE_HOSTNAME=${__GLOBALID}

ROOT=/etc/openvpn
SERVER_CONFIG=${ROOT}/minke-server.ovpn
CLIENT_CONFIG=${ROOT}/minke-client.ovpn
ORIGINAL_CLIENT_CONFIG=/etc/config.ovpn
PORTRANGE_START=41310
PORTRANGE_LEN=256
TTL=600 # 10 minutes
TTL2=300 # TTL/2
OTP_SERVER=${ROOT}/server.otp
OTP_CLIENT=${ROOT}/client.otp


if [ "${MODE}" != "SERVER" -a "${MODE}" != "CLIENT" ]; then
  echo "MODE not set"
  exit 1
fi
if [ "${MODE}" = "CLIENT" -a ! -e ${ORIGINAL_CLIENT_CONFIG} ]; then
  echo "Missing client config for CLIENT mode"
  exit 1
fi

# External interface
EXTERNAL_NET=10.20.30.0
EXTERNAL_MASK=255.255.255.248
SERVER_IP=10.20.30.1
CLIENT_IP=10.20.30.2

HOME_IP=$(ip addr show dev ${HOME_INTERFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)
INTERNAL_IP=$(ip addr show dev ${INTERNAL_INTERFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)

PATH=$PATH:/usr/share/easy-rsa

# Clear ports
cp /dev/null /etc/status/forwardports.txt

# Prime random
RANDOM=$(head -1 /dev/urandom | cksum)

if [ "${MODE}" = "SERVER" ]; then

  CONFIG=${SERVER_CONFIG}

  export EASYRSA_VARS_FILE=/etc/easyrsa.vars

  # Generate server config
  if [ ! -e ${ROOT}/server-done ]; then
    cd ${ROOT}
    rm -rf *
    easyrsa init-pki
    EASYRSA_BATCH=1 easyrsa build-ca nopass
    easyrsa gen-dh
    openvpn --genkey --secret pki/ta.key
    easyrsa build-server-full minke nopass
    easyrsa gen-crl
    touch server-done
    cd /
  fi

  if [ ! -e ${SERVER_CONFIG} ]; then

    # Generate one-time-password exchange
    cat /proc/sys/kernel/random/uuid > ${OTP_SERVER}
    cat /proc/sys/kernel/random/uuid > ${OTP_CLIENT}

    # Select an unused port within our standard range
    active_ports=$(upnpc -m ${HOME_INTERFACE} -L | grep "^ *\d\? UDP\|TCP .*$" | sed "s/^.*:\(\d*\).*$/\1/")
    while true ; do
      PORT=$((${PORTRANGE_START} + RANDOM % ${PORTRANGE_LEN}))
      if ! $(echo $active_ports | grep -q ${PORT}); then
        break;
      fi
    done

    # Generate the client config
    cd ${ROOT}
    easyrsa build-client-full minke-client nopass
    cd /
    echo "client
nobind
dev ${EXTERNAL_INTERFACE}
persist-tun
persist-key
resolv-retry
remote-cert-tls server
remote ${PRIVATE_HOSTNAME}.${DDNS_DOMAIN} ${PORT} ${PROTO}
key-direction 1
cipher AES-256-CBC
auth SHA256
ncp-ciphers AES-256-GCM
push-peer-info
<key>
$(cat ${ROOT}/pki/private/minke-client.key)
</key>
<cert>
$(cat ${ROOT}/pki/issued/minke-client.crt)
</cert>
<ca>
$(cat ${ROOT}/pki/ca.crt)
</ca>
<tls-auth>
$(cat ${ROOT}/pki/ta.key)
</tls-auth>
script-security 2
up /scripts/vpn-up.sh
setenv MODE CLIENT
setenv CONFIG ${CLIENT_CONFIG}
setenv PORT ${PORT}
setenv HOME_INTERFACE ${HOME_INTERFACE}
setenv EXTERNAL_INTERFACE ${EXTERNAL_INTERFACE}
setenv INTERNAL_INTERFACE ${INTERNAL_INTERFACE}
setenv EXTERNAL_REMOTE_IP ${SERVER_IP}
setenv UV_OTP $(cat ${OTP_CLIENT})" > ${CLIENT_CONFIG}

    # Make it retrievable
    cat ${CLIENT_CONFIG} > ${ORIGINAL_CLIENT_CONFIG}

    # Generate the server config
    echo "port ${PORT}
#local ${HOME_IP}
proto ${PROTO}
dev ${EXTERNAL_INTERFACE}
key-direction 0
<ca>
$(cat ${ROOT}/pki/ca.crt)
</ca>
<cert>
$(cat ${ROOT}/pki/issued/minke.crt)
</cert>
<key>
$(cat ${ROOT}/pki/private/minke.key)
</key>
<tls-auth>
$(cat ${ROOT}/pki/ta.key)
</tls-auth>
<dh>
$(cat ${ROOT}/pki/dh.pem)
</dh>
topology subnet
server ${EXTERNAL_NET} ${EXTERNAL_MASK}
persist-tun
persist-key
cipher AES-256-CBC
auth SHA256
ncp-ciphers AES-256-GCM
keepalive 10 30
script-security 2
up /scripts/vpn-up.sh
setenv MODE SERVER
setenv CONFIG ${SERVER_CONFIG}
setenv ROOT ${ROOT}
setenv PORT ${PORT}
setenv HOME_INTERFACE ${HOME_INTERFACE}
setenv EXTERNAL_INTERFACE ${EXTERNAL_INTERFACE}
setenv INTERNAL_INTERFACE ${INTERNAL_INTERFACE}
setenv EXTERNAL_REMOTE_IP ${CLIENT_IP}
setenv SERVER_CONFIG ${SERVER_CONFIG}
setenv CLIENT_CONFIG ${CLIENT_CONFIG}
setenv OTP_CLIENT ${OTP_CLIENT}
setenv OTP_SERVER ${OTP_SERVER}
client-connect /scripts/client-connect.sh
push \"setenv-safe OTP $(cat ${OTP_SERVER})\"" > ${SERVER_CONFIG}

  fi

  # Extract port from server config
  PORT=$(grep "^port " ${SERVER_CONFIG} | sed "s/port \(\d\+\)/\1/")

  # Add in the PORTS we need to open in the firewall
  cp ${CONFIG} /tmp/config.ovpn
  echo "setenv PORTMAX ${PORTMAX}" >> /tmp/config.ovpn
  echo "push \"setenv-safe PORTMAX ${PORTMAX}\"" >> /tmp/config.ovpn
  for p in $(seq 0 ${PORTMAX}); do
    VAL=$(eval "echo \$PORT_${p}")
    echo "setenv PORT_${p} ${VAL}" >> /tmp/config.ovpn
    echo "push \"setenv-safe PORT_${p} ${VAL}\"" >> /tmp/config.ovpn
  done

else

  # No client config file yet (missing or zero-length), copy in a configured one
  if [ ! -s ${CLIENT_CONFIG} ]; then
    cp ${ORIGINAL_CLIENT_CONFIG} ${CLIENT_CONFIG}
  fi

  CONFIG=${CLIENT_CONFIG}

  # Extract port from client config
  remote=$(grep '^remote ' ${CLIENT_CONFIG})
  remote=${remote#* * }
  PORT=${remote%% *}

  # Add in the PORTS we need to open in the firewall
  cp ${CONFIG} /tmp/config.ovpn
  echo "setenv PORTMAX ${PORTMAX}" >> /tmp/config.ovpn
  echo "setenv UV_PORTMAX ${PORTMAX}" >> /tmp/config.ovpn
  for p in $(seq 0 ${PORTMAX}); do
    VAL=$(eval "echo \$PORT_${p}")
    echo "setenv PORT_${p} ${VAL}" >> /tmp/config.ovpn
    echo "setenv UV_PORT_${p} ${VAL}" >> /tmp/config.ovpn
  done

fi

# DNS
echo "
user=root
no-hosts
hostsdir=/etc/dnshosts.d/
conf-dir=/etc/dnsmasq.d/,*.conf
resolv-file=/etc/dnsmasq_resolv.conf
address=/local/
clear-on-reload
" > /etc/dnsmasq.conf

openvpn --daemon --config /tmp/config.ovpn

if [ "${MODE}" = "SERVER" ]; then
  trap "upnpc -m ${HOME_INTERFACE} -d ${PORT} ${PROTO}; killall sleep openvpn; exit" TERM INT
  # Open the NAT
  sleep 1 &
  while wait "$!"; do
    upnpc -e ${HOSTNAME} -m ${HOME_INTERFACE} -a ${HOME_IP} ${PORT} ${PORT} ${PROTO} ${TTL}
    sleep ${TTL2} &
  done
  upnpc -m ${HOME_INTERFACE} -d ${PORT} ${PROTO}
else
  trap "killall sleep openvpn; exit" TERM INT
  sleep 2147483647d &
  wait "$!"
fi
