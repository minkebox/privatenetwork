#! /bin/sh

# Create PORTS nat list.
iptables -t nat -N PORTS
iptables -t nat -A PREROUTING -i ${EXTERNAL_INTERFACE} -j PORTS

# Allow traffic in and out if we've started a connection out
iptables -A INPUT  -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT -i ${HOME_INTERFACE}
# Any traffic which arrives on the home network is immediately forwarded to the other end of the private
# network except if its traffic for the OpenVPN.
if [ "${MODE}" = "SERVER" ]; then
  iptables -t nat -A PREROUTING -p udp --dport ${PORT} -j ACCEPT -i ${HOME_INTERFACE}
else
  iptables -t nat -A PREROUTING -p udp --sport ${PORT} -j ACCEPT -i ${HOME_INTERFACE}
fi
iptables -t nat -A PREROUTING  -j DNAT --to-destination ${EXTERNAL_REMOTE_IP} -i ${HOME_INTERFACE}

# Masquarade outgoing traffic on all networks. This hides the internals of the routing from everyone.
iptables -t nat -A POSTROUTING -j MASQUERADE -o ${EXTERNAL_INTERFACE}
iptables -t nat -A POSTROUTING -j MASQUERADE -o ${INTERNAL_INTERFACE}
iptables -t nat -A POSTROUTING -j MASQUERADE -o ${HOME_INTERFACE}

/usr/sbin/dnsmasq

if [ "${MODE}" = "CLIENT" ]; then
  if [ "${OPENVPN_OTP}" != "" ]; then
    # Stash OTP key
    if ! grep -q "setenv UV_OTP ${OPENVPN_OTP}" ${CONFIG}; then
      echo "setenv UV_OTP ${OPENVPN_OTP}" >> ${CONFIG}
    fi
  fi
  # Stash the PORTS
  cp /dev/null /etc/status/forwardports.txt
  for p in $(seq 0 ${OPENVPN_PORTMAX}); do
    v=$(eval "echo \$OPENVPN_PORT_${p}")
    echo -n "${v} " >> /etc/status/forwardports.txt
  done
fi


# Cycle thought the ports and setup forwarding
for p in $(seq 0 ${PORTMAX}); do
  v=$(eval "echo \$PORT_${p}")
  ip=$(echo ${v} | cut -d':' -f1)
  port=$(echo ${v} | cut -d':' -f2)
  protocol=$(echo ${v} | cut -d':' -f3)
  if [ ${port} != 0 ]; then
    iptables -t nat -A PORTS -p ${protocol} --dport ${port} -j DNAT --to-destination ${ip}:${port} -i ${EXTERNAL_INTERFACE}
  fi
done
