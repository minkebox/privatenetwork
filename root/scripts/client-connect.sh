#! /bin/sh

invalidate() {
  rm -f ${OTP_CLIENT} ${OTP_SERVER}
  EASYRSA_BATCH=1 easyrsa revoke minke-client
  easyrsa gen-crl
  rm -f ${CLIENT_CONFIG} ${SERVER_CONFIG} \
    ${ROOT}/pki/reqs/minke-client.req ${ROOT}/pki/private/minke-client.key ${ROOT}/pki/issued/minke-client.crt
  reboot
}

# If we have an OTP_CLIENT key, then the UV_OTP key *must* match
if [ -e ${OTP_CLIENT} ]; then
  if [ "${UV_OTP}" = "$(cat ${OTP_CLIENT})" ]; then
    # Key match - so this is the first time we've seen the client.
    # Remove keys so this can never happen again. In future a valid client will
    # always present the server key
    rm ${OTP_CLIENT}
    grep -v "setenv-safe OTP" ${SERVER_CONFIG} > ${SERVER_CONFIG}.tmp
    mv ${SERVER_CONFIG}.tmp ${SERVER_CONFIG}
  else
    # Key doesn't match - so this network has been compromised - delete the config
    invalidate
  fi
elif [ -e ${OTP_SERVER} ]; then
  if [ "${UV_OTP}" != "$(cat ${OTP_SERVER})" ]; then
    # Key doesn't match - so this network has been compromised - delete the config
    invalidate
  fi
else
  # No keys - reset
  invalidate
fi

# Stash the PORTS
cp /dev/null /etc/status/forwardports.txt
for p in $(seq 0 ${UV_PORTMAX}); do
  v=$(eval "echo \$UV_PORT_${p}")
  echo -n "${v} " >> /etc/status/forwardports.txt
done
