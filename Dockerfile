FROM alpine:latest

RUN apk add openvpn easy-rsa miniupnpc dnsmasq ;\
    rm -f /etc/openvpn/*

COPY root/ /

EXPOSE 53/tcp 53/udp
VOLUME /etc/openvpn /etc/status

ENTRYPOINT ["/startup.sh"]
